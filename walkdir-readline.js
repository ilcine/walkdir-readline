// -----------------------
// Directory and file read
// -----------------------

//read arguments begin // use: node start --dir veriler --tab 2
// -- default --dir argument is "veriler"; 
// -- default --tab argument is "2"; DISABLED

// -- choose file dir --
const dirIndex = process.argv.indexOf('--dir'); // isEmpty=-1; noEmpty first parm is 2
let dirValue; if (dirIndex > -1) { dirValue = process.argv[dirIndex + 1]; }
const dir = (dirValue || 'veriler');
//console.log("directory :", dir);

// --- choose tab delimiter number ---- **DISABLED**
const tabIndex = process.argv.indexOf('--tab'); // isEmpty=-1; noEmpty first parm is 2
let tabValue; if (tabIndex > -1) { tabValue = process.argv[tabIndex + 1]; }
const tab = (tabValue || '2');
//console.log("delimiter tab :", tab);

// arguments end 


const fs = require('fs'), path = require('path'), rl = require('readline');

// write variable
var logger = fs.createWriteStream('export.txt', {
  flags: 'a' // 'a' means appending (old data will be preserved)
})
	
// --- Main loop ( Read directory ) ---	//
walkDir(dir, function(filePath) {
	
	const fileContents = fs.readFileSync(filePath, 'utf8');
	
	// Get file's creation time
	stats = fs.statSync(filePath);
	current_datetime = new Date();
	
	var lineRead = rl.createInterface({
	  input: fs.createReadStream(filePath),
		console: false
	});
	
	// --- tpl content loop begin ( Read file content ) --- //
	var tpl = "";		 
	lineRead.on('line', (line) => {  

		var splitArr = line.split("\t"); //  two elements
		
		// skip single column 		
		if ( splitArr.length > 1) { 
			//tpl += splitArr[1].replace(/ /g,'') +"\t" + splitArr[2].replace(/ /g,'') +"\t";  // merge two elements to tpl
			tpl += splitArr[1].trim() +"\t"+ splitArr[2].trim() +"\t";  // merge two elements to tpl
		}
	 
	}).on('close', function() {	
		logger.write( 
		// path.basename(filePath) +"\t"+ 
		filePath +"\t"+
		stats.birthtime.toISOString().replace(/T/, ' ').replace(/\..+/, '') +"\t"+ 
		tpl + "\n" 
		);	  
	}); // --- tpl content loop end ---
	
}); // Main loop end

// -- walk function --
function walkDir(dir, callback) {
  fs.readdirSync(dir).forEach( f => {
    let dirPath = path.join(dir, f);
    let isDirectory = fs.statSync(dirPath).isDirectory();
    isDirectory ? 
      walkDir(dirPath, callback) : callback(path.join(dir, f));
  }); 
};


