# Walkdir-readline

## Install

`git clone https://gitlab.com/ilcine/walkdir-readline.git`

`cd walkdir-readline`

other install: `npm i walkdir-readline` and `mv node_modules/walkdir-readline/ .` and `cd walkdir-readline`

## run

`node walkdir-readline`

## run with arguments

// -- default --dir argument is "veriler"

// -- default --tab argument is "2" DISABLED  

`node walkdir-readline --dir [directory name] --tab [tab delimiter size]` 

ex:

`node walkdir-readline --dir veriler --tab 2` 
